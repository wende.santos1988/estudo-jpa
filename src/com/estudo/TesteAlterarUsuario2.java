package com.estudo;

import com.estudo.jpa.model.basico.Usuario;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TesteAlterarUsuario2 {

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence
                .createEntityManagerFactory("exercicios-jpa");
        EntityManager manager = factory.createEntityManager();
        EntityTransaction transaction = manager.getTransaction();

        manager.getTransaction().begin();

        Usuario usuario = manager.find(Usuario.class, 2L);
        usuario.setNome("Henrique Santos");
        usuario.setEmail("hsantos@teste.com");
        manager.merge(usuario);

        manager.getTransaction().commit();

        manager.close();
        factory.close();
    }

}
