package com.estudo;

import com.estudo.jpa.model.basico.Usuario;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TesteNovoUsuario {

    public static void main(String[] args) {
        Usuario usuario = new Usuario("Paula Santos", "test@ndo.com");

        EntityManagerFactory factory = Persistence
                .createEntityManagerFactory("exercicios-jpa");
        EntityManager manager = factory.createEntityManager();

        EntityTransaction transaction = manager.getTransaction();

        transaction.begin();
        manager.persist(usuario);
        transaction.commit();

        manager.close();
        factory.close();
    }

}
