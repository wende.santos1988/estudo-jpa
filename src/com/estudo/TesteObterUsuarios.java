package com.estudo;

import com.estudo.jpa.model.basico.Usuario;

import javax.persistence.*;
import java.util.List;

public class TesteObterUsuarios {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence
                .createEntityManagerFactory("exercicios-jpa");
        EntityManager manager = factory.createEntityManager();

        String jpql = "select u from Usuario u";
        TypedQuery<Usuario> query = manager.createQuery(jpql, Usuario.class);
        query.setMaxResults(5);

        List<Usuario> usuarios = query.getResultList();

        usuarios.forEach(usuario -> {
            System.out.println("ID: " + usuario.getId()
                + " - nome: " + usuario.getNome());
        });

        manager.close();
        factory.close();
    }
}
