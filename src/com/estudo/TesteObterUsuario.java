package com.estudo;

import com.estudo.jpa.model.basico.Usuario;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TesteObterUsuario {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence
                .createEntityManagerFactory("exercicios-jpa");
        EntityManager manager = factory.createEntityManager();

        Usuario usuario = manager.find(Usuario.class, 1L);
        System.out.println(usuario.getNome());

        manager.close();
        factory.close();
    }

}
